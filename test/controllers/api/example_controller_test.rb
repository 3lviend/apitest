require 'test_helper'

class Api::ExampleControllerTest < ActionController::TestCase
  test "the xml request to render json format" do
    get 'index'
    assert_response :success
    body = JSON.parse(response.body)
    assert_equal "a", body["0"]["name"]
  end
end
