class Api::ExampleController < ApplicationController
  NEW_VALUE = ["name", "some_string", "some_date", "iso_code", "price"]

	def index
		#setup the URI to localhost with port 3333
		uri = URI('http://127.0.0.1:3333/')

		#do a request using net/http and parse the xml result using nokogiri
        response = Net::HTTP.get(uri)
        xml_doc  = Nokogiri::XML(response)
        
        #fetching the xml doc and put the parameter/key and value to hash format then render to json format
        hash = {}
        parse_xml(xml_doc, "example").each_with_index do |doc, index|
           hash[index] = {}
           ["name", "param-1", "param-2", "some-code", "some-price-param"].each_with_index do |id, i|
             hash[index][NEW_VALUE[i]] = parse_xml(doc, id)[0].content
           end
        end

        render :json => hash
	end

  #nokogiri method to search an id from xml doc
	def parse_xml(xml_doc, id)
      xml_doc.xpath("//"+id)
	end
end
